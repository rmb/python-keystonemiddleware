Source: python-keystonemiddleware
Section: python
Priority: optional
Maintainer: Debian OpenStack <team+openstack@tracker.debian.org>
Uploaders:
 Thomas Goirand <zigo@debian.org>,
 Corey Bryant <corey.bryant@canonical.com>,
 Michal Arbet <michal.arbet@ultimum.io>,
Build-Depends:
 debhelper-compat (= 10),
 dh-python,
 openstack-pkg-tools,
 python3-all,
 python3-pbr,
 python3-setuptools,
 python3-sphinx,
Build-Depends-Indep:
 bandit,
 python3-bandit,
 python3-coverage,
 python3-cryptography,
 python3-docutils,
 python3-fixtures,
 python3-hacking,
 python3-keystoneauth1,
 python3-keystoneclient,
 python3-memcache,
 python3-openstackdocstheme,
 python3-oslo.cache,
 python3-oslo.config,
 python3-oslo.context,
 python3-oslo.i18n,
 python3-oslo.log,
 python3-oslo.messaging,
 python3-oslo.serialization,
 python3-oslo.utils,
 python3-oslotest,
 python3-pycadf,
 python3-requests,
 python3-requests-mock,
 python3-six,
 python3-sphinxcontrib.apidoc,
 python3-sphinxcontrib.svg2pdfconverter,
 python3-stestr,
 python3-stevedore,
 python3-testresources,
 python3-testtools,
 python3-webob,
 python3-webtest,
 subunit,
Standards-Version: 4.5.1
Vcs-Browser: https://salsa.debian.org/openstack-team/libs/python-keystonemiddleware
Vcs-Git: https://salsa.debian.org/openstack-team/libs/python-keystonemiddleware.git
Homepage: https://launchpad.net/keystonemiddleware

Package: python-keystonemiddleware-doc
Section: doc
Architecture: all
Depends:
 ${misc:Depends},
 ${sphinxdoc:Depends},
Description: Middleware for OpenStack Identity (Keystone) - doc
 This package contains middleware modules designed to provide authentication
 and authorization features to web services other than Keystone. The most
 prominent module is keystonemiddleware.auth_token. This package does not
 expose any CLI or Python API features.
 .
 This package contains the documentation.

Package: python3-keystonemiddleware
Architecture: all
Depends:
 python3-keystoneauth1 (>= 3.12.0),
 python3-keystoneclient (>= 1:3.20.0),
 python3-oslo.cache,
 python3-oslo.config,
 python3-oslo.context,
 python3-oslo.i18n,
 python3-oslo.log,
 python3-oslo.serialization,
 python3-oslo.utils,
 python3-pbr,
 python3-pycadf,
 python3-requests,
 python3-six,
 python3-webob,
 ${misc:Depends},
 ${python3:Depends},
Description: Middleware for OpenStack Identity (Keystone) - Python 3.x
 This package contains middleware modules designed to provide authentication
 and authorization features to web services other than Keystone. The most
 prominent module is keystonemiddleware.auth_token. This package does not
 expose any CLI or Python API features.
 .
 This package contains the Python 3.x module.
